drush cssflip 7.x-1.x-dev

A Drush plugin to flip CSS files. RTL to LTR and vice versa.

First bone to have the <strong> drush cssflip</strong> command work to flip css files .

For example if we want to switch a css file for the tweme to rtl.

We can apply the following drush command <strong>drush csslist</strong> to get the list of all css files in the current enabled theme.

<code>
rajab@vardot:/var/www/drush_cssflip/sites/all$ drush csslist
8 CSS Files in theme : tweme
sites/all/themes/tweme/assets/css/bootstrap-extra.css
sites/all/themes/tweme/assets/css/drupal.css
sites/all/themes/tweme/assets/css/style.css
sites/all/themes/tweme/assets/css/bootstrap-extra-rtl.css
sites/all/themes/tweme/assets/css/max-width-940.css
sites/all/themes/tweme/assets/css/prettify.css
</code>

We can select a file to translate for example style.css the path to the file will be sites/all/themes/tweme/assets/css/style.css as you can see in the list of files after having the command <strong>drush csslist</strong> 


So that the command to flip the file will be :

<strong>drush cssflip rtl sites/all/themes/tweme/assets/css/style.css</strong>

<code>
rajab@vardot:/var/www/drush_cssflip/sites/all$ drush cssflip rtl sites/all/themes/tweme/assets/css/style.css
run drush_cssflip_rtl for sites/all/themes/tweme/assets/css/style.css
Had a cssflip rtl
</code>

Now if you run drush csslist again you will have the file

sites/all/themes/tweme/assets/css/style-rtl.css

We are in the way to enhance the way this tool work.