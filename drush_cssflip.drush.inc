<?php

require_once dirname(__FILE__) . '/CDrushCSSFlip.inc';

/**
 * Implements hook_drush_command().
 */
function drush_cssflip_drush_command() {

  $items = array();
  $items['cssflip'] = array(
    'description' => dt('CSS Flip From LTR to RTL format.'),
    'arguments'   => array(
      'direction' => dt('CSS flip direction'),
      'file'      => dt('CSS File name and path'),
    ),
    'examples' => array(
      'Standard example' => 'drush cssflip rtl',
      'Argument example' => 'drush cssflip rtl style.css',
    ),
    'aliases' => array('cssflip'),
  );
  
  $items['csslist'] = array(
    'description' => dt('CSS list of files for current enabled theme'),
    'examples' => array(
      'Standard example' => 'drush csslist',
    ),
    'aliases' => array('csslist'),
  );

  return $items;
}

/**
 * #RA:2013-09-15: cssflip drush command.
 * @param type $direction
 * @param type $file
 */
function drush_drush_cssflip_cssflip($direction = 'rtl', $file = '') {

  if (isset($direction)) {
    switch ($direction) {
      case 'rtl':
      {
        if (isset($file)) {
          _drush_cssflip_rtl($file);
        }
        break;
      }
      case 'ltr':
      {
        if (isset($file)) {
          _drush_cssflip_ltr($file);
        }
        break;
      }
    default :
    {
      drush_print('Error 001 : cssflip support only rtl or ltr direction for css flipping.');
      drush_log('Error 001 : cssflip support only rtl or ltr direction for css flipping.', 'error');
      break;
    }
    }
    
  }
}


/**
 * #RA:2013-09-15: drush cssflip rtl function implimentation.
 * @param type $file
 */
function _drush_cssflip_rtl ($file = '') {
    drush_print("run drush_cssflip_rtl for " . $file);
    $look_up_error = 0;
    
    
			if( is_file($file) ) { 
				// Read the css file
				$f = fopen($file, 'r');
				$c = fread($f, filesize($file));
				fclose($f);
	
				$drush_cssflip = new CDrushCSSFlip;
				$file_data = $drush_cssflip->parse_css($c); 
        
        drush_cssflip_save($file, $file_data);
        
      }
      else {
        $look_up_error = 1;
      }
    
    _drush_cssflip_rtl_lookup_errors($look_up_error);
}

/**
 * #RA:2013-09-15: drush cssflip ltr function implimentation.
 * @param type $file
 */
function _drush_cssflip_ltr ($file = '') {
    drush_print("run drush_cssflip_ltr for " . $file);
    $look_up_error = 0;
    
    
    
    _drush_cssflip_ltr_lookup_errors($look_up_error);
}


/**
 * #RA:2013-09-15: cssflip ltr lookup errors messages.
 * @param type $look_up_error
 */
function _drush_cssflip_ltr_lookup_errors($look_up_error = -1) {
  if ($look_up_error == 0) {
    // No errors.
    drush_log('Had a cssflip ltr', 'ok');
  }
  
  
  
}

/**
 * #RA:2013-09-15: cssflip rtl lookup errors messages.
 * @param type $look_up_error
 */
function _drush_cssflip_rtl_lookup_errors($look_up_error = -1) {
  if ($look_up_error == 0) {
    // No errors.
    drush_log('Had a cssflip rtl', 'ok');
  }
  elseif ($look_up_error == 1) {
    drush_log('css file is not exist .. try drush csslist to get list of css ' .
              'files for the current active theme', 'error');
  }
  
  
  
}


/**
 *  #RA:2013-09-21: drush csslist command.
 *                  to list all css files in the current active theme.
 */
function drush_drush_cssflip_csslist() {
  
  $current_dir = drupal_get_path('theme', $GLOBALS['theme']);
  $css_files = file_scan_directory($current_dir, '/.*\.css$/');
  
  $count_css_files = count($css_files);
  if($count_css_files > 0) {
    drush_print($count_css_files . ' CSS Files in theme : ' . $GLOBALS['theme']);
    foreach ($css_files as $css_file) {
      drush_print($css_file->uri);
    }
  }
  else {
    drush_print($count_css_files . ' CSS Files in the current active theme');
  }
}


function drush_cssflip_save($file, $file_data) {
  if($file_data) {
    // now, save.
      $error = false;
      $_file = preg_match( '/^(.*\\/)?\.css$/', $file ) ? substr($file, 0, -9) . 'rtl.css' : substr($file, 0, -4) . '-rtl.css';

      if( is_file( $_file ) ) {

        // file exists so rename it.
        $__file = substr( $_file, 0, -4 ) . '.bak.css';
        $__file_b = substr( $_file, 0, -4 ) . '.bak-%%.css';

        $n = 0;
        while( is_file( $__file ) ) {
          $__file = str_replace( '%%', $n, $__file_b );
          $n ++;
        }

        unset( $n );

        rename($_file, $__file) or $error = true;
      }

      if( !$error ) {
        $fp = fopen($_file, 'w');
        if( $fp ) {
          // write new file
          fwrite( $fp, $file_data, strlen( $file_data ) );
          fclose( $fp );
        } else {
          $error = true;
          drush_log('Error saving  file not writable.');
        }
      }

  }
}