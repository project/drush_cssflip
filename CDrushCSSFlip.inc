<?php

class CDrushCSSFlip {
	
	/**
	 * these bools are used to see if we add a (|padding-|margin-|border-)(right|left) so we can
	 * 		 set the other direction's value. if both directions are added then we add nothing.
	 *
	 * has   => right or left
	 * has_p => padding
	 * has_m => margin
	 * has_b => border
	 */
	var $has   = false;
	var $has_p = false;
	var $has_m = false;
	var $has_b = false;
	
	/**
	 * parse one line of css, if it has something that can be RTLed then do our job.
	 * anyway, if not, return false.
	 */
	function parse_line( $line ) {
		// check if it has right or left word.
		if( preg_match( '/(right|left)/', $line ) ) {
			
			// if it's right; 5px for example, we set $has to 'right'
			// if $has is left, then we reset it to false.
			if( preg_match( '/^([\s\t]*)(right|left)\:/', $line ) ) {
				$is_right = preg_match( '/^([\s\t]*)right\:/', $line );
				if( $this->has === ($is_right?'left':'right') ) {
					$this->has = false;
				} else {
					$this->has = $is_right?'right':'left';
				}
			}
			
			// same as the above code
			if( preg_match( '/^([\s\t]*)padding-(right|left)\:/', $line ) ) {
				$is_right = preg_match( '/^([\s\t]*)padding-right\:/', $line );
				if( $this->has_p === ($is_right?'left':'right') ) {
					$this->has_p = false;
				} else {
					$this->has_p = $is_right?'right':'left';
				}
			}
			
			// same as the above code
			if( preg_match( '/^([\s\t]*)margin-(right|left)\:/', $line ) ) {
				$is_right = preg_match( '/^([\s\t]*)margin-right\:/', $line );
				if( $this->has_m === ($is_right?'left':'right') ) {
					$this->has_m = false;
				} else {
					$this->has_m = $is_right?'right':'left';
				}
			}
			
			// same as the above code
			if( preg_match( '/^([\s\t]*)border-(right|left)\:/', $line ) ) {
				$is_right = preg_match( '/^([\s\t]*)border-right\:/', $line );
				if( $this->has_b === ($is_right?'left':'right') ) {
					$this->has_b = false;
				} else {
					$this->has_b = $is_right?'right':'left';
				}
			}
			
			// flip right snf left.
			$line = $this->right_to_left( $line );
			
		} elseif( preg_match( '/(padding|margin):(([\s\t]*)([^\s\t]+)([\s\t]+)([^\s\t]+)([\s\t]+)([^\s\t]+)([\s\t]+)([^\s\t]*)([\s\t]*)(!important)?([\s\t]*);)/', $line, $matches ) ) {
			// If it's <code>padding: 1 2 3 4;</code> we'll flip the 2nd and the 4th values.
			
			// if they are equal, return false.
			if( $matches[6] == $matches[10] )
			
				$line = false;
				
			else
			
				// now flip
				$line = str_replace( $matches[2], $matches[3].$matches[4].$matches[5].$matches[10].$matches[7].$matches[8].$matches[9].$matches[6].$matches[11].$matches[11].';', $line );
				
		} else { // no RTL to do, return false
			$line = false;
		}
		
		// return the result.
		return $line;
	}
	
	/**
	 * explode block to lines, call $this->parse_line on each,
	 * then add neccesary code to the end of block;
	 */
	function parse_block( $block ) {
		
		// reset some vars
		$this->has   = false;
		$this->has_p = false;
		$this->has_m = false;
		$this->has_b = false;
		
		// explode to lines
		$block = explode( ";", $block );
		
		// prepare return array
		$return = array();
		
		// loop
		foreach( $block as $line ) {
			$line = preg_replace('/\\/\\*.*\\*\\//', '', $line); // remove comments
			if( !$line ) continue;
			$line = trim($line) . ';';
			preg_replace( '/^[\s\t]*([a-z\-]+)\:[\s\t]*(.+)[\s\t]*;/', '$1: $2;', $line );
			$c = $this->parse_line( $line );
			if( $c ) {
				$return[] = '	'.$c;
			}
		}
		
		// check for right/left
		if( $this->has ) {
			$t = ($this->has   === 'right' ) ? $this->has   : 'left';
			$return[] = "\t$t: auto;";
		}
		
		// check for padding
		if( $this->has_p ) {
			$t = ($this->has_p === 'right' ) ? $this->has_p : 'left';
			$return[] = "\tpadding-$t: 0;";
		}
		
		// check for margin
		if( $this->has_m ) {
			$t = ($this->has_m === 'right' ) ? $this->has_m : 'left';
			$return[] = "\tmargin-$t: auto;";
		}
		
		// check for border
		if( $this->has_b ) {
			$t = ($this->has_b === 'right' ) ? $this->has_b : 'left';
			$return[] = "\tborder-$t: none;";
		}
		
		// return
		return count($return) ? implode("\n", $return) : false;
	}
	
	/**
	 * extract blocks from css file, then $this->parse_block() on each.
	 */
	function parse_css($css) {
		
		/**
		 * TODO: store comments so if comment includes {} we don't get confused :S
		 */
		$comments = array();
		
		$b = explode( '}', $css );
		
		/**
		 * this return array contains values in the form:
		 * 				array( $selector, $code );
		 */
		$return = array();
		
		// media vals
		$is_media = false;
		$media_selector = '';
		$media_i = 0;
		
		// loop throw blocks.
		foreach( $b as $_b ) {
			
			// explode to selector and code.
			$_b = explode( '{', $_b );
			
			// check header to see if it's @media.
			$h = $this->remove_comments($_b[0]);
			
			if( preg_match( '/@media/', $h ) ) {
				
				$is_media = true;
				$media_selector = $_b[0];
				$media_i = 0;
				
				array_shift($_b);
				
			} elseif( count($_b) == 1 && $is_media ) {
				
				if( $media_i ) {
					$return = array_slice( $return, 0, -$media_i );
					$a = array_slice( $return, -$media_i, $media_i );
					
					$s = '';
					
					// loop throw the array
					foreach( $a as $_a ) {
						if( $_a[1] )
							$s .= "\n" . trim( $_a[0] ) . " {\n$_a[1]\n}\n";
						else
							$s .= "\n" . $_a[0];
					}
					
					$return[] = array( $media_selector, $s );
					
				} else {
					
					
					// or i'll keep the selector!
					$c = $media_selector;
					
					if( !empty( $c ) ) {
						$return[] = array( $c , '' );
					}
				}
				
				$is_media = false;
				$madia_selector = '';
				$media_i = 0;
				
				continue;
			} elseif( preg_match( '/^\\.f[rl]$/', trim($h) ) ) {
				
				//leave comments alone!
				$c = $this->keep_comments($_b[0]);
				if( !empty( $c ) ) {
					$return[] = array( $c );
				}
				
				// continue
				continue;
			}
			
			// parse code
			$t = $this->parse_block( $_b[1] );
			
			// add to the $return array
			if( $t ) {
				$media_i++;
				$return[] = array( $this->right_to_left($_b[0]), $t );
			} else {
				
				//leave comments alone!
				$c = $this->keep_comments($_b[0]);
				if( !empty( $c ) ) {
					$return[] = array( $c );
				}
				
			}
		}
		
		// return string
		$x = '';
		
		// loop throw the array
		foreach( $return as $r ) {
			if( count($r)>1 )
				$x .= "\n" . trim( $r[0] ) . " {\n$r[1]\n}\n";
			else
				$x .= "\n" . $r[0];
		}
		
		//remove 3+ empty lines
		$x = preg_replace( '/(\n)\n+/', '$1$1', $x );
		
		// first char is an empty line!
		$x = preg_replace( '/^\n+/', '', $x );
		
		if( empty($x) )
			return false;
		
		// Done. whew!
		return $x;
	}
	
	/**
	 * remove the css comments from a string.
	 */
	function remove_comments($string) {
		
		// first, remove the //comments
		$s = explode( "\n", $string );
		$r = array();
		foreach( $s as $_s ) {
			$_s = trim( $_s );
			if( substr( $_s, 0, 2 ) != '//' ) {
				$r[] = $_s;
			}
		}
		$s = implode( "\n", $r );
		
		// now, remove the /*comments*/
		$s = explode( '*/', $s );
		$r = array();
		foreach( $s as $_s ) {
			$t = explode( '/*', $_s );
			if( !empty( $t[0] ) ) {
				$r[] = $t[0];
			}
		}
		
		// and return
		return implode( "\n", $r );
	}
	
	/**
	 * remove everything except the comments from a string.
	 */
	function keep_comments($string) {
		
		// look for /*comments*/
		$s = explode( '*/', $string );
		$x = '';
		
		foreach( $s as $_s ) {
			$t = explode( '/*', $_s );
			if( count( $t )>1 ) {
				$x .= "/*{$t[1]}*/\n";
			}
		}
		
		// and return
		return $x;
	}
	
	/**
	 * replace "right" width "left" and vice versa
	 */
	function right_to_left($str) {
		
		// replace left with a TMP string.
		$s = str_replace( 'left', 'TMP_LEFT_STR', $str );
		
		// flip right to left.
		$s = str_replace( 'right', 'left', $s );
		
		// flip left to right.
		$s = str_replace( 'TMP_LEFT_STR', 'right', $s );
		
		// return
		return $s;
	}
  
  /**
   * #RA:2013-09-21
	 * replace "left" width "right" and vice versa
	 */
	function left_to_right($str) {
		
		// replace right with a TMP string.
		$s = str_replace( 'right', 'TMP_LEFT_STR', $str );
		
		// flip left to right.
		$s = str_replace( 'left', 'right', $s );
		
		// flip right to left.
		$s = str_replace( 'TMP_LEFT_STR', 'left', $s );
		
		// return
		return $s;
	}
	
}